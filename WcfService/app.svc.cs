﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfService.App_Code.DBAccess;
using WcfService.App_Code.Entity;
using System.ServiceModel.Channels;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class app : Iapp
    {
        public LoginResponse Login(LoginRequest Request)
        {
            LoginResponse response = null;
            String ipaddress = String.Empty;
            
            ipaddress = this.GetIp();

            using (SessionDao dao = new SessionDao())
            {
                response = dao.Login(Request, ipaddress, Guid.NewGuid().ToString());
            }

            return response;
        }


        public Response Logout(Request Request)
        {
            return null;
        }


        public CreateResponse CustomerRequest(CreateRequest Request)
        {
            return null;
        }


        public QueryCustomerResponse QueryCustomer(QueryCustomerRequest Request)
        {
            return null;
        }


        private String GetIp()
        {
            OperationContext context = null;
            MessageProperties messageProperties = null;
            RemoteEndpointMessageProperty endpointProperty = null;
            String ip = null;

            context = OperationContext.Current;

            messageProperties = context.IncomingMessageProperties;
            endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

            ip = endpointProperty.Address;

            return ip;
        }

    }
}
