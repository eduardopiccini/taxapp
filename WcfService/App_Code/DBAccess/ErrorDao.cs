﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace WcfService.App_Code.DBAccess
{
    public class ErrorMessageRequest
    {
        public String Message { get; set; }
    }

    public class ErrorMessageResponse
    {
        public long Code { get; set; }

        public String Message { get; set; }
    }

    public class ErrorDao : IDisposable
    {
        public ErrorMessageResponse GetError(ErrorMessageRequest request)
        {
            ErrorMessageResponse response = new ErrorMessageResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("on_error_code", OracleDbType.Int64).Direction = System.Data.ParameterDirection.Output;
                cmd.Parameters.Add("oz_error_message", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("iz_error_message", OracleDbType.Varchar2, 2000).Value = request.Message;

                DataManager.RunDataCommandSP(ref cmd, "tc_pg_error.error_message", String.Empty, String.Empty);

                response.Code = Common.ObjectToInt32(cmd.Parameters["on_error_code"]);
                response.Message = Common.ObjectToString(cmd.Parameters["oz_error_message"]);
                
            }
            catch (Exception)
            {
                response.Code = Common.GENERAL_ERROR_CODE;
                response.Message = Common.GENERAL_ERROR_MESSAGE;
            }

            return response;
        }

        public void Dispose()
        {
        }
    }
}