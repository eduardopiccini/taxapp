﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using WcfService.App_Code.Entity;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace WcfService.App_Code.DBAccess
{

    [DataContract]
    public class CreateRequest
    {

        [DataMember]
        public int CompanyId { get; set; }

        [DataMember]
        public String CustomerId { get; set; }

        [DataMember]
        public Double CustomerLatitude { get; set; }

        [DataMember]
        public Double CustomerLongitude { get; set; }

        [DataMember]
        public int TotalTravelers { get; set; }

        [DataMember]
        public int GroupId { get; set; }
    }

    [DataContract]
    public class CreateResponse
    {
        public CreateResponse()
        {
            this.Response = new Response();
        }

        public Response Response { get; set; }

        public long RequestId { get; set; }
    }

    public class RequestDao
    {

        public CreateResponse Create(CreateRequest request)
        {
            CreateResponse response = new CreateResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {

                cmd.Parameters.Add("in_company_id", OracleDbType.Int32).Value = request.CompanyId;
                cmd.Parameters.Add("iz_customer_id", OracleDbType.Varchar2, 200).Value = request.CustomerId;
                cmd.Parameters.Add("in_customer_latitude", OracleDbType.Double).Value = request.CustomerLatitude;
                cmd.Parameters.Add("in_customer_longitude", OracleDbType.Double).Value = request.CustomerLongitude;
                cmd.Parameters.Add("in_total_travelers", OracleDbType.Int32).Value = request.TotalTravelers;
                cmd.Parameters.Add("in_group_id", OracleDbType.Int32).Value = request.GroupId;
                cmd.Parameters.Add("on_request_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_error_message", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "tc_pg_request.register", String.Empty, String.Empty);

                response.Response.Code = Common.ObjectToInt32(cmd.Parameters["on_error_code"].Value);
                response.Response.Message = Common.ObjectToString(cmd.Parameters["oz_error_message"].Value);

                if (response.Response.Code.Equals(Common.APPROVED_CODE))
                {
                    response.RequestId = Common.ObjectToInt64(cmd.Parameters["on_request_id"].Value);
                }

            }
            catch (Exception ex)
            {
                using (ErrorDao dao = new ErrorDao())
                {
                    ErrorMessageRequest req = new ErrorMessageRequest();
                    ErrorMessageResponse res = null;

                    req.Message = ex.Message;
                    res = dao.GetError(req);

                    response.Response.Code = res.Code;
                    response.Response.Message = res.Message;
                }
            }

            return response;
        }

    }
}