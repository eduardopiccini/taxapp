﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using WcfService.App_Code.Entity;
using Oracle.DataAccess.Client;
using System.Data;
using OracleAccessLayer;

namespace WcfService.App_Code.DBAccess
{
    [DataContract]
    public class LoginResponse
    {
        public LoginResponse()
        {
            this.Response = new Response();
        }

        [DataMember]
        public String SessionId { get; set; }

        [DataMember]
        public Response Response { get; set; }

        [DataMember]
        public long TaxiId { get; set; }

        [DataMember]
        public String Name { get; set; }

        [DataMember]
        public String LastName { get; set; }

        [DataMember]
        public String PhoneNumber { get; set; }


        [DataMember]
        public Status Status { get; set; }
    }


    [DataContract]
    public class LoginRequest
    {
        [DataMember]
        public String UserName { get; set; }

        [DataMember]
        public String Password { get; set; }

        [DataMember]
        public int CompanyId { get; set; }
    }

    public class ValidateSessionResponse
    {
        public ValidateSessionResponse()
        {
            this.Response = new Response();
        }

        public Response Response { get; set; }

        public long TaxiId { get; set; }

        public String UserId { get; set; }
    }

    public class SessionDao : IDisposable
    {
        public LoginResponse Login(LoginRequest request, String ip, String sessionId)
        {
            LoginResponse response = new LoginResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_api_user_id", OracleDbType.Varchar2, 20).Value = request.UserName;
                cmd.Parameters.Add("iz_api_password", OracleDbType.Varchar2, 200).Value = request.Password;
                cmd.Parameters.Add("in_company_id", OracleDbType.Int64).Value = request.CompanyId;
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 300).Value = sessionId;
                cmd.Parameters.Add("iz_ip_address", OracleDbType.Varchar2, 300).Value = ip;
                cmd.Parameters.Add("on_taxi_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_name", OracleDbType.Varchar2, 45).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_last_name", OracleDbType.Varchar2, 45).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_phone_number", OracleDbType.Varchar2, 45).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_status_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_status_name", OracleDbType.Varchar2, 45).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_error_message", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "tc_pg_session.login", String.Empty, String.Empty);
                
                response.Response.Code = Common.ObjectToInt32(cmd.Parameters["on_error_code"].Value);
                response.Response.Message = Common.ObjectToString(cmd.Parameters["oz_error_message"].Value);

                if (response.Response.Code.Equals(Common.APPROVED_CODE))
                {
                    response.TaxiId = Common.ObjectToInt32(cmd.Parameters["on_taxi_id"].Value);
                    response.Name = Common.ObjectToString(cmd.Parameters["oz_name"].Value);
                    response.LastName = Common.ObjectToString(cmd.Parameters["oz_last_name"].Value);
                    response.PhoneNumber = Common.ObjectToString(cmd.Parameters["oz_phone_number"].Value);
                    response.Status.Id = Common.ObjectToInt32(cmd.Parameters["on_status_id"].Value);
                    response.Status.Description = Common.ObjectToString(cmd.Parameters["oz_status_name"].Value);
                }
                
            }
            catch (Exception ex)
            {
                using(ErrorDao dao = new ErrorDao())
	            {
                    ErrorMessageRequest req = new ErrorMessageRequest();
                    ErrorMessageResponse res = null;
                    
                    req.Message = ex.Message;
                    res = dao.GetError(req);

                    response.Response.Code = res.Code;
                    response.Response.Message = res.Message;
                } 
            }

            return response;
        }

        public Response Logout(Request request)
        {
            Response response = new Response();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 300).Value = request.SessionId;
                cmd.Parameters.Add("on_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_error_message", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "tc_pg_session.logout", String.Empty, String.Empty);

                response.Code = Common.ObjectToInt32(cmd.Parameters["on_error_code"].Value);
                response.Message = Common.ObjectToString(cmd.Parameters["oz_error_message"].Value);

            }
            catch (Exception ex)
            {
                using (ErrorDao dao = new ErrorDao())
                {
                    ErrorMessageRequest req = new ErrorMessageRequest();
                    ErrorMessageResponse res = null;

                    req.Message = ex.Message;
                    res = dao.GetError(req);

                    response.Response.Code = res.Code;
                    response.Response.Message = res.Message;
                }
            }

            return response;
        }

        public ValidateSessionResponse ValidateSession(String sessionId)
        {
            ValidateSessionResponse response = new ValidateSessionResponse();
            OracleCommand cmd = new OracleCommand();

            try
            {
                cmd.Parameters.Add("iz_session_id", OracleDbType.Varchar2, 300).Value = sessionId;
                cmd.Parameters.Add("on_taxi_id", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_api_user_id", OracleDbType.Varchar2, 20).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("on_error_code", OracleDbType.Int64).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("oz_error_message", OracleDbType.Varchar2, 3000).Direction = ParameterDirection.Output;

                DataManager.RunDataCommandSP(ref cmd, "tc_pg_session.validate_session", String.Empty, String.Empty);

                response.Response.Code = Common.ObjectToInt32(cmd.Parameters["on_error_code"].Value);
                response.Response.Message = Common.ObjectToString(cmd.Parameters["oz_error_message"].Value);

                if (response.Response.Code.Equals(Common.APPROVED_CODE))
                {
                    response.TaxiId = Common.ObjectToInt32(cmd.Parameters["on_taxi_id"].Value);
                    response.UserId = Common.ObjectToString(cmd.Parameters["oz_api_user_id"].Value);
                }

            }
            catch (Exception ex)
            {
                using (ErrorDao dao = new ErrorDao())
                {
                    ErrorMessageRequest req = new ErrorMessageRequest();
                    ErrorMessageResponse res = null;

                    req.Message = ex.Message;
                    res = dao.GetError(req);

                    response.Response.Code = res.Code;
                    response.Response.Message = res.Message;
                }
            }

            return response;
        }

        public void Dispose()
        {

        }
    }
}