﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfService.App_Code.Entity
{
    [DataContract]
    public class Response
    {
        [DataMember]
        public long Code { get; set; }

        [DataMember]
        public String Message { get; set; }
    }
}