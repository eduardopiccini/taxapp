﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.App_Code
{
    public class Common
    {
        public const long GENERAL_ERROR_CODE = -1;
        public const String GENERAL_ERROR_MESSAGE = "TRY AGAIN LATER";

        public const long APPROVED_CODE = 0;
        public const String APPROVED_MESSAGE = "SUCCESS";

        public static String ObjectToString(Object obj)
        {
            if (obj != null)
            {
                return obj.ToString();
            }
            else
            {
                return String.Empty;
            }
        }

        public static Int32 ObjectToInt32(Object obj)
        {
            if (obj != null)
            {
                return Convert.ToInt32(obj.ToString());
            }
            else
            {
                return 0;
            }
        }

        public static Int64 ObjectToInt64(Object obj)
        {
            if (obj != null)
            {
                return Convert.ToInt64(obj.ToString());
            }
            else
            {
                return 0;
            }
        }
    }
}