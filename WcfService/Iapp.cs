﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfService.App_Code;
using WcfService.App_Code.DBAccess;
using WcfService.App_Code.Entity;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface Iapp
    {

        [OperationContract]
        LoginResponse Login(LoginRequest Request);

        [OperationContract]
        Response Logout(Request Request);

        [OperationContract]
        CreateResponse CustomerRequest(CreateRequest Request);

        [OperationContract]
        QueryCustomerResponse QueryCustomer(QueryCustomerRequest Request);

        //[OperationContract]
        //Response RegisterTaxiLocation(RegisterTaxiLocationRequest Request);

        //[OperationContract]
        //Response AcceptRequest(AcceptCustomerRequest Request);

        //[OperationContract]
        //Response CancelRequest(CancelCustomerRequest Request);

        //[OperationContract]
        //Response ChangeTaxiStatus(ChangeTaxiStatusRequest Request);
    }

    #region Request

    

    [DataContract]
    public class CreateRequest
    {
        public CreateRequest()
        {
            this.Request = new Request();
            this.Location = new Location();
        }

        [DataMember]
        public Request Request { get; set; }

        [DataMember]
        public int CompanyId { get; set; }

        [DataMember]
        public String CustomerId { get; set; }

        [DataMember]
        public Location Location { get; set; }

        [DataMember]
        public int TotalTravelers { get; set; }

    }

    [DataContract]
    public class QueryCustomerRequest
    {
        public QueryCustomerRequest()
        {
            this.Request = new Request();
        }

        [DataMember]
        public Request Request { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public int CompanyId { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public int TotalTravelers { get; set; }
    }

    [DataContract]
    public class RegisterTaxiLocationRequest
    {
        public RegisterTaxiLocationRequest()
        {
            this.Request = new Request();
            this.Location = new Location();
        }

        [DataMember]
        public int TaxiId { get; set; }

        [DataMember]
        public Request Request { get; set; }

        [DataMember]
        public Location Location { get; set; }
    }

    [DataContract]
    public class AcceptCustomerRequest
    {
        [DataMember]
        public Request Request { set; get; }

        [DataMember]
        public int Id { get; set; }
    }

    [DataContract]
    public class CancelCustomerRequest
    {
        [DataMember]
        public Request Request { set; get; }

        [DataMember]
        public int Id { get; set; }
    }

    [DataContract]
    public class ChangeTaxiStatusRequest
    {
        [DataMember]
        public Request Request { get; set; }

        [DataMember]
        public int TaxiId { get; set; }

        [DataMember]
        public int StatusId { get; set; }
    }

    #endregion

   


    #region Response
    
    

    [DataContract]
    public class CreateResponse
    {
        public CreateResponse()
        {
            this.Response = new Response();
            this.Status = new Status();
        }

        [DataMember]
        public Response Response { get; set; }

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public Status Status { get; set; }

        [DataMember]
        public TaxiInformation TaxiInformation { get; set; }
    }

    [DataContract]
    public class QueryCustomerResponse
    {
        public QueryCustomerResponse()
        {
            this.Response = new Response();
        }

        [DataMember]
        public Response Response { get; set; }

        [DataMember]
        public List<CustomerInformation> CustomerInformation { get; set; }
    }

    #endregion




    #region Others



    [DataContract]
    public class Color
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class Model
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class Makes
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class Company
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Name { get; set; }
    }

    [DataContract]
    public class TaxiInformation
    {
        public TaxiInformation()
        {
            this.Color = new Color();
            this.Model = new Model();
            this.Makes = new Makes();
            this.Company = new Company();
            this.Location = new Location();
        }

        [DataMember]
        public long TaxiId { get; set; }

        [DataMember]
        public Color Color {get;set;}

        [DataMember]
        public Model Model { get; set; }

        [DataMember]
        public Makes Makes { get; set; }

        [DataMember]
        public Company Company { get; set; }

        [DataMember]
        public String FleetCode { get; set; }

        [DataMember]
        public Location Location { get; set; }
    }

    [DataContract]
    public class CustomerInformation
    {
        public CustomerInformation()
        {
            this.Status = new Status();
            this.Location = new Location();
        }

        [DataMember]
        public long RequestId { get; set; }

        [DataMember]
        public String CustomerId { get; set; }

        [DataMember]
        public Location Location { get; set; }

        [DataMember]
        public int TotalTravelers { get; set; }

        [DataMember]
        public String RequestDateTime { get; set; }

        [DataMember]
        public Status Status { get; set; }
    }

    [DataContract]
    public class Status
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public String Description { get; set; }
    }

    [DataContract]
    public class Location
    {
        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public double Longitude { get; set; }
    }

    #endregion

   
}
