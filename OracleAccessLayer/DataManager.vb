Imports Microsoft.VisualBasic
Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types
Imports System.Data
Imports System
Imports System.Configuration
Imports System.Text


''' <summary>
''' Clase para manejar la conexion a la Base de Datos utilizando Oracle Data Provider for .NET
''' </summary>
''' <remarks></remarks>
Public Class DataManager

    ''' <summary>
    ''' Retorna la cadena de conexion(ConnectionString)
    ''' </summary>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConnStr(ByVal Usuario As String, ByVal Password As String) As String
        Dim StrConn As New Text.StringBuilder(ConfigurationManager.AppSettings("ParamConn"))

        If Not String.IsNullOrEmpty(Usuario) And Not String.IsNullOrEmpty(Password) Then
            StrConn.Append("User ID=" & Usuario)
            StrConn.Append(";Password=" & Password)
        End If

        Return StrConn.ToString
    End Function

    ''' <summary>
    ''' Destruye la conexion de un objeto OracleConexion
    ''' </summary>
    ''' <param name="Conn">Objeto OracleConnection</param>
    ''' <param name="LimpiarPool">Limpia el Pool de la conexi�n</param>
    ''' <remarks></remarks>
    Public Shared Sub DisposeConnection(ByRef Conn As OracleConnection, Optional ByVal LimpiarPool As Boolean = False)
        If Conn IsNot Nothing AndAlso Not Conn.State = ConnectionState.Closed Then

            'Limpiamos el Pool de la conexion
            If LimpiarPool Then OracleConnection.ClearPool(Conn)

            Conn.Close()
            Conn.Dispose()
        End If
    End Sub

    ''' <summary>
    ''' Limpia la conexion del Pool si ocurre alguno de los errores de Oracle manejados
    ''' </summary>
    ''' <param name="Conn"></param>
    ''' <param name="Ex"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function LimpiarPoolError(ByVal Conn As OracleConnection, ByVal Ex As Exception) As Boolean
        Dim ErrorClearPool() As String = ConfigurationManager.AppSettings("ErrorClearPool").Split(",")

        For Each S As String In ErrorClearPool
            If Ex.Message.IndexOf(S) > -1 Then
                DisposeConnection(Conn, True)

                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Limpia la conexion del Pool
    ''' </summary>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <remarks></remarks>
    Public Shared Sub LimpiarPoolConn(ByVal Usuario As String, ByVal Password As String)
        Dim Conn As OracleConnection = New OracleConnection(ConnStr(Usuario, Password))
        Conn.Open()

        DisposeConnection(Conn, True)
    End Sub

    ''' <summary>
    ''' Retorna un objeto DataTable con los datos de la ejecucion de una consulta de SQL
    ''' </summary>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDataTable(ByVal strSQL As String, ByVal Usuario As String, ByVal Password As String, Optional ByVal ReIntento As Boolean = False) As DataTable
        Return GetDataTable(New OracleCommand, strSQL, False, Usuario, Password, False)
    End Function

    ''' <summary>
    ''' Retorna un DataTable con los datos de la ejecucion de una consulta de SQL cuando se utilicen variables Bind
    ''' </summary>
    ''' <param name="Cmd"></param>
    ''' <param name="strSQL"></param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDataTable(ByRef Cmd As OracleCommand, ByVal strSQL As String, ByVal Usuario As String, ByVal Password As String) As DataTable
        Return GetDataTable(Cmd, strSQL, False, Usuario, Password, False)
    End Function

    ''' <summary>
    ''' Retorna un DataTable con los datos de la ejecucion de una consulta de SQL cuando se utilicen variables Bind
    ''' </summary>
    ''' <param name="Cmd"></param>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="ValidarWhere">Indica que el Query debe tener por lo menos una condici�n</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDataTable(ByRef Cmd As OracleCommand, ByVal strSQL As String, ByVal ValidarWhere As Boolean, ByVal Usuario As String, ByVal Password As String) As DataTable
        Return GetDataTable(Cmd, strSQL, ValidarWhere, Usuario, Password, False)
    End Function

    ''' <summary>
    ''' Retorna un DataTable con los datos de la ejecucion de una consulta de SQL cuando se utilicen variables Bind
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="ValidarWhere">Indica que el Query debe tener por lo menos una condici�n</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <remarks></remarks>
    Public Shared Function GetDataTable(ByRef Cmd As OracleCommand, ByVal strSQL As String, ByVal ValidarWhere As Boolean, ByVal Usuario As String, ByVal Password As String, ByVal ReIntento As Boolean) As DataTable
        Dim Conn As OracleConnection = Nothing

        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))
            Conn.Open()

            Cmd.CommandText = strSQL

            If ValidarWhere Then
                If strSQL.ToUpper.IndexOf("WHERE") < 0 Then Throw New Exception("Debe enviar algun parametro de b�squeda.")
            End If

            Cmd.Connection = Conn

            Dim Da As New OracleDataAdapter(Cmd)
            Dim dt As New DataTable
            'Dim ds As New DataSet

            Da.Fill(dt)



            Return dt
        Catch ex As Exception
            If Not ReIntento AndAlso LimpiarPoolError(Conn, ex) Then
                Return GetDataTable(Cmd, strSQL, Usuario, Password, True)
            Else
                Throw New Exception(ex.Message)
            End If
        Finally
            DisposeConnection(Conn)
        End Try
    End Function


    ''' <summary>
    ''' Retorna un DataTable con los datos de la ejecucion de una consulta de SQL cuando se utilicen variables Bind
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="ValidarWhere">Indica que el Query debe tener por lo menos una condici�n</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <remarks></remarks>
    Public Shared Function GetDataTable(ByRef Cmd As OracleCommand, ByVal strSQL As String, ByVal startRowIndex As String, ByVal maxRows As String, ByVal ValidarWhere As Boolean, ByVal Usuario As String, ByVal Password As String, ByVal ReIntento As Boolean) As DataTable
        Dim Conn As OracleConnection = Nothing

        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))
            Conn.Open()

            Cmd.CommandText = strSQL

            If ValidarWhere AndAlso Cmd.Parameters.Count = 0 Then Throw New Exception("Debe enviar algun parametro de b�squeda.")

            Cmd.Connection = Conn

            Dim Da As New OracleDataAdapter(Cmd)
            Dim dt As New DataTable
            Dim ds As New DataSet

            Da.Fill(ds, startRowIndex, maxRows, "dt")

            Return ds.Tables("dt")
        Catch ex As Exception
            If Not ReIntento AndAlso LimpiarPoolError(Conn, ex) Then
                Return GetDataTable(Cmd, strSQL, Usuario, Password, True)
            Else
                Throw New Exception(ex.Message)
            End If
        Finally
            DisposeConnection(Conn)
        End Try
    End Function


    Public Shared Function GetDataTableSP(ByRef Cmd As OracleCommand, ByVal CmdText As String, ByVal Usuario As String, ByVal Password As String, ByVal NomParamRefCursor As String) As DataTable
        Dim ds As DataSet

        ds = GetDataSetSP(Cmd, CmdText, Usuario, Password, NomParamRefCursor)

        If Not ds Is Nothing Then
            If ds.Tables.Count > 0 Then
                Return ds.Tables(0)
            End If
        End If

        Return Nothing
    End Function

    Public Shared Function GetDataSetSP(ByRef Cmd As OracleCommand, ByVal CmdText As String, ByVal Usuario As String, ByVal Password As String, ByVal ParamArray NomParamRefCursor() As String) As DataSet
        Dim Conn As OracleConnection = Nothing

        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))
            Conn.Open()

            Cmd.CommandText = CmdText
            Cmd.Connection = Conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.ExecuteNonQuery()

            Dim Da As New OracleDataAdapter()
            Dim Ds As New DataSet



            For Each P As String In NomParamRefCursor
                If Not DirectCast(Cmd.Parameters(P).Value, Oracle.DataAccess.Types.OracleRefCursor).IsNull Then
                    Ds.Tables.Add(New DataTable(P))

                    If Not TypeOf Cmd.Parameters(P).Value Is Oracle.DataAccess.Types.OracleRefCursor Then
                        'Throw New Exception("El valor del par�metro " & P & " no es de tipo RefCursor")
                        Continue For
                    End If

                    Da.Fill(Ds, P, CType(Cmd.Parameters(P).Value, Oracle.DataAccess.Types.OracleRefCursor))
                End If
            Next

            Return Ds
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            DisposeConnection(Conn)
        End Try
    End Function



    ''' <summary>
    ''' Retorna el primer campo del primer registro de la ejecucion del query. 
    ''' Los campos o registros adicionales son ignorados. Nota: Si el resultado es nulo se retorna un espacio vacio (' ')
    ''' </summary>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetExecuteScalar(ByVal strSQL As String, ByVal Usuario As String, ByVal Password As String, Optional ByVal ReIntento As Boolean = False) As Object
        Return GetExecuteScalar(New OracleCommand, strSQL, Usuario, Password, False)
    End Function

    ''' <summary>
    ''' Retorna el primer campo del primer registro de la ejecuci�n del query cuando se utilicen variables Bind. 
    ''' Los campos o registros adicionales son ignorados. Nota: Si el resultado es nulo se retorna un espacio vacio (' ')
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetExecuteScalar(ByRef Cmd As OracleCommand, ByVal strSQL As String, ByVal Usuario As String, ByVal Password As String, Optional ByVal ReIntento As Boolean = False) As Object
        Dim Conn As OracleConnection = Nothing

        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))
            Conn.Open()

            Cmd.Connection = Conn
            Cmd.CommandText = strSQL

            Dim Resultado As Object = Cmd.ExecuteScalar

            If IsDBNull(Resultado) OrElse Resultado Is Nothing Then
                Return " "
            Else
                Return Resultado
            End If
        Catch ex As Exception
            If Not ReIntento AndAlso LimpiarPoolError(Conn, ex) Then
                Return GetExecuteScalar(Cmd, strSQL, Usuario, Password, True)
            Else
                Throw New Exception(ex.Message)
            End If
        Finally
            Cmd.Dispose()
            DisposeConnection(Conn)
        End Try
    End Function

    ''' <summary>
    ''' Ejecuta un Command con una setencia de SQL
    ''' </summary>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <remarks></remarks>
    Public Shared Sub RunDataCommand(ByVal strSQL As String, ByVal Usuario As String, ByVal Password As String, Optional ByVal ReIntento As Boolean = False)
        RunDataCommand(New OracleCommand, strSQL, Usuario, Password, False)
    End Sub

    ''' <summary>
    ''' Ejecuta un Command con una setencia de SQL cuando se utilicen variables Bind
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="strSQL">Sentencia de SQL</param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <remarks></remarks>
    Public Shared Sub RunDataCommand(ByRef Cmd As OracleCommand, ByVal strSQL As String, ByVal Usuario As String, ByVal Password As String, Optional ByVal ReIntento As Boolean = False)
        Dim Conn As OracleConnection = Nothing

        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))
            Conn.Open()

            Cmd.Connection = Conn
            Cmd.CommandText = strSQL

            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            If Not ReIntento AndAlso LimpiarPoolError(Conn, ex) Then
                RunDataCommand(Cmd, strSQL, Usuario, Password, True)
            Else
                Throw New Exception(ex.Message)
            End If
        Finally
            Cmd.Dispose()
            DisposeConnection(Conn)
        End Try
    End Sub

    ''' <summary>
    ''' Ejecuta un Command con un Stored Procedure utilizando Transaction
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="CmdText">Stored procedure o funcion a ejecutar</param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <remarks></remarks>
    ''' 
       Public Shared Sub RunDataCommandSP(ByRef Cmd As OracleCommand, ByVal CmdText As String, ByRef Ds As DataSet, ByVal nomParRefCursor() As String, ByVal ReIntento As Boolean, ByVal Usuario As String, ByVal Password As String)
        Dim Conn As OracleConnection = Nothing
        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))
            Conn.Open()

            Cmd.CommandText = CmdText
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.Connection = Conn

            Cmd.ExecuteNonQuery()

            If Not nomParRefCursor Is Nothing Then
                For Each parName As String In nomParRefCursor
                    If (Cmd.Parameters(parName).OracleDbType = OracleDbType.RefCursor) Then

                        Dim odap As New OracleDataAdapter()
                        Ds.Tables.Add(New DataTable(parName))
                        If Not CType(Cmd.Parameters(parName).Value, OracleRefCursor).IsNull Then odap.Fill(Ds, parName, CType(Cmd.Parameters(parName).Value, OracleRefCursor))
                    End If
                Next
            End If

        Catch ex As Exception
            If Not ReIntento AndAlso LimpiarPoolError(Cmd.Connection, ex) Then
                RunDataCommandSP(Cmd, CmdText, True, Usuario, Password)
            Else
                Throw New Exception(ex.Message)
            End If
        Finally
            DisposeConnection(Conn)
        End Try
    End Sub

    Public Shared Sub RunDataCommandSP(ByRef Cmd As OracleCommand, ByVal CmdText As String, ByVal Usuario As String, ByVal Password As String)
        RunDataCommandSP(Cmd, CmdText, Nothing, Nothing, False, Usuario, Password)
    End Sub
    Public Shared Sub RunDataCommandSP(ByRef Cmd As OracleCommand, ByVal CmdText As String, ByVal ReIntento As Boolean, ByVal Usuario As String, ByVal Password As String)
        Try
            RunDataCommandSP(Cmd, CmdText, Nothing, Nothing, ReIntento, Usuario, Password)
        Catch ex As Exception
            If Not ReIntento AndAlso LimpiarPoolError(Cmd.Connection, ex) Then
                RunDataCommandSP(Cmd, CmdText, True, Usuario, Password)
            Else
                Throw New Exception(ex.Message)
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Agregar al Command los parametros de errores
    ''' </summary>
    ''' <param name="Cmd">OracleCommand al que se le agregaran los parametros</param>
    ''' <remarks></remarks>
    Public Shared Sub AddCmdParamError(ByVal Cmd As OracleCommand)
        If Cmd.Parameters("Error") Is Nothing Then
            Cmd.Parameters.Add("Error", OracleDbType.Int32, 1)
            Cmd.Parameters.Add("Error_Message", OracleDbType.Varchar2, 255)

            Cmd.Parameters("Error").Direction = ParameterDirection.Output
            Cmd.Parameters("Error_Message").Direction = ParameterDirection.Output
        End If

    End Sub

    ''' <summary>
    ''' Metodo que convierte a un DataTable los parametros de salida de un Command de Oracle
    ''' </summary>
    ''' <param name="Cmd"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertirCmd_Dt(ByVal Cmd As OracleCommand) As DataTable
        Return ConvertirCmd_Dt(Cmd, False)
    End Function

    Public Shared Function ConvertirCmd_Dt(ByVal Cmd As OracleCommand, ByVal SoloTipoTable As Boolean) As DataTable
        Return ConvertirCmd_Dt(Cmd, Nothing, SoloTipoTable)
    End Function
    Public Shared Function ConvertirCmd_Dt(ByVal Cmd As OracleCommand, ByVal nomParRefCursor As Array, ByVal SoloTipoTable As Boolean) As DataTable

        Return ConvertirCmd_Dt(Cmd, nomParRefCursor, SoloTipoTable, "")
    End Function

    Public Shared Function ConvertirCmd_Dt(ByVal Cmd As OracleCommand, ByVal nomParRefCursor As Array, ByVal SoloTipoTable As Boolean, ByVal RetornarSoloParametro As String) As DataTable
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim dr As DataRow
        Dim P As OracleParameter
        Dim NumReg As Integer = 0 'Indica la cantidad de registros de la tabla

        'Insertamos todas los parametros como columnas al DataTable
        For x As Integer = 0 To Cmd.Parameters.Count - 1
            P = Cmd.Parameters(x)

            If Not P.Direction = ParameterDirection.Input Then
                If SoloTipoTable AndAlso Not IsArray(P.Value) Then
                    Continue For
                End If

                If IsArray(P.Value) AndAlso NumReg = 0 Then NumReg = CType(P.Value, Array).Length
                If P.ParameterName <> RetornarSoloParametro AndAlso RetornarSoloParametro.Trim <> "" Then

                    Continue For
                Else
                    If IsArray(P.Value) Then NumReg = CType(P.Value, Array).Length
                End If


                Select Case P.DbType
                    Case DbType.Date, DbType.DateTime, DbType.Time
                        dt.Columns.Add(P.ParameterName, GetType(Date))
                    Case DbType.Byte, DbType.Int16, DbType.Int32, DbType.Int64, DbType.SByte, DbType.VarNumeric
                        dt.Columns.Add(P.ParameterName, GetType(Long))
                    Case DbType.Double, DbType.Decimal, DbType.Currency, DbType.Single
                        dt.Columns.Add(P.ParameterName, GetType(Double))
                    Case DbType.Object
                        dt.Columns.Add(P.ParameterName, GetType(Object))
                    Case Else
                        dt.Columns.Add(P.ParameterName, GetType(String))
                End Select
            End If
        Next

        'Ponemos a NumReg = 1 para insertar un registro con los parametros que no son tipo table
        If NumReg = 0 AndAlso Not SoloTipoTable Then NumReg = 1

        'Lleno el DataTable con los datos de los parametros tipo Table
        For f As Integer = 0 To NumReg - 1
            dr = dt.NewRow

            Dim Col As Integer = 0 ' Indica la columna en la cual se va insertar el valor

            For c As Integer = 0 To Cmd.Parameters.Count - 1
                P = Cmd.Parameters(c)

                Dim Valor As Object = DBNull.Value

                If Not P.Direction = ParameterDirection.Input Then
                    If SoloTipoTable AndAlso Not IsArray(P.Value) Then
                        Continue For
                    End If
                    If P.ParameterName <> RetornarSoloParametro AndAlso RetornarSoloParametro.Trim <> "" Then Continue For

                    'If IsArray(P.Value) Then
                    '    If f < CType(P.Value, Array).Length Then
                    '        If TypeOf CType(P.Value, Array)(f) Is Oracle.DataAccess.Types.OracleString Then
                    '            Valor = IIf(CType(P.Value(f), Oracle.DataAccess.Types.OracleString).IsNull, DBNull.Value, CType(P.Value, Array)(f))
                    '        Else
                    '            Valor = CType(P.Value, Array)(f)
                    '        End If
                    '    End If
                    'ElseIf TypeOf P.Value Is OracleDate AndAlso Not IsDBNull(P.Value) Then
                    '    Valor = CType(P.Value, OracleDate).Value
                    'ElseIf TypeOf P.Value Is OracleString Then
                    '    Valor = P.Value.ToString
                    'Else
                    '    Valor = P.Value
                    'End If

                    'dr(Col) = Valor
                    'Col += 1
                    If IsArray(P.Value) AndAlso f < CType(P.Value, Array).Length Then
                        Valor = CType(P.Value, Array)(f)
                    Else
                        Valor = IIf(IsArray(P.Value), DBNull.Value, Common.SiValorNulo(P.Value, DBNull.Value))
                    End If

                    If TypeOf Valor Is OracleDate AndAlso Not CType(Valor, OracleDate).IsNull Then
                        dr(Col) = CType(Valor, OracleDate).Value
                    ElseIf TypeOf Valor Is OracleString AndAlso Not CType(Valor, OracleString).IsNull Then
                        dr(Col) = CType(Valor, OracleString).Value
                    ElseIf TypeOf Valor Is OracleDecimal AndAlso Not CType(Valor, OracleDecimal).IsNull() Then

                        dr(Col) = Convert.ToDouble(Valor.value)

                    ElseIf Not TypeOf Valor Is OracleDate AndAlso Not TypeOf Valor Is OracleString AndAlso Not TypeOf Valor Is OracleDecimal Then

                        dr(Col) = Valor

                    End If

                    Col += 1
                End If
            Next
            dt.Rows.Add(dr)
        Next


        dt.TableName = "datos"
        Return dt
    End Function

    ''' <summary>
    ''' Metodo que retorna un parametro de Oracle del tipo Table. 
    ''' Nota: El tipo de dato 'Table' es un arreglo
    ''' </summary>
    ''' <param name="Nombre">Nombre del parametro</param>
    ''' <param name="oraTipo">Tipo del parametro</param>
    ''' <param name="CantMaxima">Cantidad maxima de elementos del arreglo</param>
    ''' <param name="Arreglo">Valores de entrada del Arreglo</param>
    ''' <param name="Direccion">Direcci�n del parametro</param>
    ''' <param name="CantCaracteres">Cantidad de caracteres de los elementos del arreglo. 
    '''  Nota: Este parametro no es necesario para los tipos de datos de Numericos(Int16, Int32, Int64, Double, Decimal, etc..) 
    ''' y de fecha (Date, TimeStamp, etc..)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CrearOraParametroTable(ByVal Nombre As String, ByVal oraTipo As OracleDbType, ByVal CantMaxima As Integer, _
                                                  Optional ByVal Arreglo As Object = Nothing, Optional ByVal Direccion As ParameterDirection = ParameterDirection.Input, _
                                                  Optional ByVal CantCaracteres As Integer = -1) As OracleParameter
        Dim P As New OracleParameter(Nombre, oraTipo, CantMaxima, Arreglo, Direccion)

        P.CollectionType = OracleCollectionType.PLSQLAssociativeArray

        If Not CantCaracteres = -1 Then
            'Configuramos el tama�o y la cantidad de caracteres de los elementos del arreglo
            P.ArrayBindSize = New Integer(CantMaxima - 1) {}

            For x As Integer = 0 To CantMaxima - 1
                P.ArrayBindSize(x) = CantCaracteres
            Next
        End If

        Return P
    End Function

    ''' <summary>
    ''' Cambia la clave de un usuario de oracle
    ''' </summary>
    ''' <param name="ClaveNueva"></param>
    ''' <param name="ClaveConfirmada"></param>
    ''' <param name="Usuario"></param>
    ''' <param name="Password"></param>
    ''' <param name="ReIntento">Indica que reintentara ejecutar el Query con un nueva conexi�n luego de limpiar la conexion del Pool</param>
    ''' <remarks></remarks>
    Public Shared Function CambiarClave(ByVal IdUsuario As String, ByVal ClaveActual As String, ByVal ClaveNueva As String, ByVal ClaveConfirmada As String, _
                                        ByVal Usuario As String, ByVal Password As String, Optional ByVal ReIntento As Boolean = False) As Boolean
        Dim Cmd As New OracleCommand
        Dim Conn As OracleConnection = Nothing

        Try
            Conn = New OracleConnection(ConnStr(Usuario, Password))

            Try
                Conn.Open()
            Catch ex As Exception
                If ex.Message.IndexOf("ORA-28001") > -1 Then 'Contrase�a expirada
                    Conn.OpenWithNewPassword(ClaveNueva)
                Else
                    Throw New Exception(ex.Message)
                End If
            End Try

            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "SAS_PASSWORD_F"
            Cmd.Connection = Conn

            Cmd.Parameters.Add("Retorno", OracleDbType.Varchar2, 2000).Direction = ParameterDirection.ReturnValue
            Cmd.Parameters.Add("vusuario", OracleDbType.Varchar2, 255).Value = IdUsuario.ToUpper
            Cmd.Parameters.Add("vpassold", OracleDbType.Varchar2, 255).Value = ClaveActual
            Cmd.Parameters.Add("vpassnew", OracleDbType.Varchar2, 255).Value = ClaveNueva
            Cmd.Parameters.Add("vpassconf", OracleDbType.Varchar2, 255).Value = ClaveConfirmada
            Cmd.Parameters.Add("vaccion", OracleDbType.Varchar2, 255).Value = "C"

            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            If Not ReIntento AndAlso ex.Message.IndexOf("ORA-01012") > -1 OrElse ex.Message.IndexOf("ORA-02396") > -1 OrElse ex.Message.IndexOf("ORA-06508") > -1 Then 'ORA-01012: not logged on / ORA-02396: exceeded maximum idle time, please connect again  / ORA-06508 could not find program unit being called.
                DisposeConnection(Conn, True)

                Return CambiarClave(IdUsuario, ClaveActual, ClaveNueva, ClaveConfirmada, Usuario, Password, True)
            Else
                Throw New Exception(ex.Message)
            End If
        Finally
            DisposeConnection(Conn)
        End Try

        If Not Cmd.Parameters("Retorno").Value.ToString = "TRUE" Then
            Throw New Exception(Cmd.Parameters("Retorno").Value.ToString)
        End If

        Return True
    End Function


#Region "Oracle"

    Enum eOperador
        Igual
        Diferente
        Mayor
        Mayor_Igual
        Menor
        Menor_Igual
        Between
        Like_Inicial
        Like_Final
        Like_Donde_Encuentre
        InIn
    End Enum

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="Valor">Valor de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal Valor As Object, ByVal DbType As OracleDbType)
        AddBindParamCmd(Cmd, StrSQL, Campo, "", Valor, "", DbType, eOperador.Igual, False)
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="Valor">Valor de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal Valor As Object, ByVal DbType As OracleDbType, ByVal Operador As eOperador)
        AddBindParamCmd(Cmd, StrSQL, Campo, "", Valor, "", DbType, Operador, False)
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="Valor1">Valor de parametro1</param>
    ''' <param name="Valor2">Valor de parametro2</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal Valor1 As Object, ByVal Valor2 As Object, ByVal DbType As OracleDbType, ByVal Operador As eOperador)
        AddBindParamCmd(Cmd, StrSQL, Campo, "", Valor1, Valor2, DbType, Operador, False)
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="NomParamValor">Indica el nombre que desea ponerle al parametro a agregar en el OracleCommand.
    ''' Nota: Este es opcional ya que dicho nombre se toma del campo. Ej.: Si el campo es "Cuenta" se crea el parametro ":vCuenta" </param>
    ''' <param name="Valor">Valor de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <param name="IncluirWhere">Indica si desea ponerle la palabra "Where" delante de la condici�n</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal NomParamValor As String, ByVal Valor As Object, ByVal DbType As OracleDbType, ByVal Operador As eOperador, ByVal IncluirWhere As Boolean)
        AddBindParamCmd(Cmd, StrSQL, Campo, NomParamValor, Valor, "", DbType, Operador, IncluirWhere)
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="Valor">Valor de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <param name="IncluirWhere">Indica si desea ponerle la palabra "Where" delante de la condici�n</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal Valor As Object, ByVal DbType As OracleDbType, ByVal Operador As eOperador, ByVal IncluirWhere As Boolean)
        AddBindParamCmd(Cmd, StrSQL, Campo, "", Valor, "", DbType, Operador, IncluirWhere)
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="Valor">Valor de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <param name="FormatoValor">Indica si el valor1 va a ser convertido o formateado al compararse. Debe poner {0} donde vaya a poner el valor.
    ''' Ej.: Para la conversion a fecha 08/07/2010 el formato es To_Date({0},'dd/mm/yyyy') --> To_Date(:vFecha,'dd/mm/yyyy')</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal Valor As Object, ByVal DbType As OracleDbType, ByVal Operador As eOperador, ByVal FormatoValor As String)
        AddBindParamCmd(Cmd, StrSQL, Campo, "", Valor, "", DbType, Operador, False, FormatoValor, "", "")
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="Valor">Valor de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <param name="FormatoValor">Indica si el valor1 va a ser convertido o formateado al compararse. Debe poner {0} donde vaya a poner el valor.
    ''' Ej.: Para la conversion a fecha 08/07/2010 el formato es To_Date({0},'dd/mm/yyyy') --> To_Date(:vFecha,'dd/mm/yyyy')</param>
    ''' <param name="FormatoCampo">Indica si al campo a compararse sera convertido o formateado.</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal Valor As Object, ByVal DbType As OracleDbType, ByVal Operador As eOperador, ByVal FormatoValor As String, ByVal FormatoCampo As String)
        AddBindParamCmd(Cmd, StrSQL, Campo, "", Valor, "", DbType, Operador, False, FormatoValor, "", FormatoCampo)
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query y crea un parametro a un OracleCommand utilizando la tecnica de Bind Variables
    ''' </summary>
    ''' <param name="Cmd">Objeto OracleCommand</param>
    ''' <param name="StrSQL">Query</param>
    ''' <param name="Campo">Nombre del campo</param>
    ''' <param name="NomParamValor">Indica el nombre que desea ponerle al parametro a agregar en el OracleCommand.
    ''' Nota: Este es opcional ya que dicho nombre se toma del campo. Ej.: Si el campo es "Cuenta" se crea el parametro ":vCuenta" </param>
    ''' <param name="Valor1">Valor1 de parametro</param>
    ''' <param name="Valor2">Valor2 de parametro</param>
    ''' <param name="DbType">Tipo de dato del campo</param>
    ''' <param name="Operador">Indica la operacion logica a realizar</param>
    ''' <param name="IncluirWhere">Indica si desea ponerle la palabra "Where" delante de la condici�n</param>
    ''' <param name="FormatoValor1">Indica si el valor1 va a ser convertido o formateado al compararse. Debe poner {0} donde vaya a poner el valor.
    ''' Ej.: Para la conversion a fecha 08/07/2010 el formato es To_Date({0},'dd/mm/yyyy') --> To_Date(:vFecha,'dd/mm/yyyy')</param>
    ''' <param name="FormatoValor2">Indica si el valor2 va a ser convertido o formateado al compararse.  Debe poner {0} donde vaya a poner el valor. 
    ''' Ej.: Para la conversion a fecha 08/07/2010 el formato es To_Date({0},'dd/mm/yyyy') --> To_Date(:vFecha,'dd/mm/yyyy')</param>
    ''' <param name="FormatoCampo">Indica si al campo a compararse sera convertido o formateado.</param>
    ''' <remarks></remarks>
    Public Shared Sub AddBindParamCmd(ByRef Cmd As OracleCommand, ByRef StrSQL As Text.StringBuilder, ByVal Campo As String, ByVal NomParamValor As String, ByVal Valor1 As Object, ByVal Valor2 As Object, _
                                      ByVal DbType As OracleDbType, ByVal Operador As eOperador, ByVal IncluirWhere As Boolean, Optional ByVal FormatoValor1 As String = "", Optional ByVal FormatoValor2 As String = "", _
                                      Optional ByVal FormatoCampo As String = "")
        If Campo = "" OrElse Valor1 Is Nothing OrElse Valor1.ToString.Trim = "" OrElse Valor1.ToString().ToLower() = "null" Then Exit Sub

            Dim NomParamValor2 As String = ""

            NomParamValor = ":v" & IIf(NomParamValor = "", Campo.Replace(".", "_").Replace(")", "").Replace("(", ""), NomParamValor).ToString.Trim

            If Operador = eOperador.Between Then
                If Valor2 Is Nothing OrElse Valor2.ToString.Trim = "" Then Throw New Exception("Para crear Bind Variables utilizando el operador Between requiere enviar 2 valores")

                NomParamValor2 = NomParamValor & "2"
                NomParamValor = NomParamValor & "1"
            End If
            If Operador = eOperador.Like_Final OrElse Operador = eOperador.Like_Donde_Encuentre OrElse Operador = eOperador.Like_Inicial Then
                Valor1 = IIf(Operador = eOperador.Like_Final OrElse Operador = eOperador.Like_Donde_Encuentre, "%", "") & Valor1 & IIf(Operador = eOperador.Like_Inicial OrElse Operador = eOperador.Like_Donde_Encuentre, "%", "")
            End If

            If DbType = OracleDbType.Date Then
                If Operador = eOperador.Igual Then
                    Operador = eOperador.Between
                    Valor2 = Valor1
                    NomParamValor2 = NomParamValor & "2"
                End If
                FormatoValor1 = "to_date({0})"
                FormatoValor2 = "to_date({0}) + .99999"
            End If

            If Cmd.Parameters(NomParamValor) Is Nothing Then Cmd.Parameters.Add(NomParamValor, DbType, Valor1, ParameterDirection.Input)


            If Not NomParamValor2 = "" AndAlso Cmd.Parameters(NomParamValor2) Is Nothing Then Cmd.Parameters.Add(NomParamValor2, DbType, Valor2, ParameterDirection.Input)

            If Not TypeOf StrSQL Is StringBuilder Then Exit Sub

            Dim Where As String = IIf(IncluirWhere, " where ", IIf(InStr(StrSQL.ToString.ToUpper, "WHERE") = 0, " Where ", " and "))
            Dim Operadores() As String = {"= {0}", "!= {0}", "> {0}", ">= {0}", "< {0}", "<= {0}", "between {0} and {1}", "like {0}", "like {0}", "like {0}", "in ({0})"}
            Dim Operacion As String = String.Format(Operadores(Operador), IIf(FormatoValor1 = "", NomParamValor, String.Format(FormatoValor1, NomParamValor)), _
                                                                          IIf(FormatoValor2 = "", NomParamValor2, String.Format(FormatoValor2, NomParamValor2)))

            StrSQL.Append(String.Format(" {0} {1} {2} ", Where, IIf(FormatoCampo = "", Campo, String.Format(FormatoCampo, Campo)), Operacion))
    End Sub

    ''' <summary>
    ''' Agrega una condicion a un Query validando que si ya tiene alguna le pone delante la palabra 'And' o en caso contrario 'Where'
    ''' </summary>
    ''' <param name="StrCond">Condicion del Query</param>
    ''' <param name="Condicion"></param>
    ''' <remarks></remarks>
    Public Shared Sub AddCondSQL(ByRef StrCond As Text.StringBuilder, ByVal Condicion As String)
        StrCond.Append(IIf(StrCond.ToString.ToLower.IndexOf("where") = -1, " where ", " and ") & Condicion & " ")
    End Sub

#End Region
End Class

