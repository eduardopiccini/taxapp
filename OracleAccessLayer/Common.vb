Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Globalization
Imports System.Net.Mail
Imports System
Imports Oracle.DataAccess.Client
Imports System.Collections.Generic

Public Enum FormatoFecha
    Cultura_Actual = 0
    Mes_Dia_Ano = 1
    Dia_Mes_Ano = 2
End Enum

Public Enum FormatoHora
    Hora_Min_AM_PM = 0
    Hora_Min_am_pm_ConPunto = 1
End Enum




Public Class Common


#Region "Validaciones y Conversiones"
    

    ''' <summary>
    ''' Evalua si el valor de una columna de un DataTable es nulo y le asigna un valor por defecto
    ''' </summary>
    ''' <param name="Dt">Objeto DataTable</param>
    ''' <param name="NomIndexColumna">Nombre/Indice de la columna</param>
    ''' <param name="ValorDefecto">Valor a ser asignado en caso de que el valor de la columna sea nulo</param>
    ''' <remarks></remarks>
    Public Shared Sub SiValorNuloColDt(ByRef Dt As DataTable, ByVal NomIndexColumna As Object, ByVal ValorDefecto As Object)
        Try
            Dim Valor As Object

            For Each dr As DataRow In Dt.Rows
                Valor = dr(NomIndexColumna)

                Valor = IIf(IsDBNull(Valor), ValorDefecto, Valor)

                dr(NomIndexColumna) = Valor
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Evalua si un valor es nulo o esta vacio y retorna un valor por defecto
    ''' </summary>
    ''' <param name="Valor"></param>
    ''' <param name="ValorDefecto">Valor a retornar en caso de que el valor sea nulo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SiValorNulo(ByVal Valor As Object, ByVal ValorDefecto As Object) As Object
        Try
            Return IIf(IsDBNull(Valor) OrElse Valor Is Nothing OrElse Valor.ToString.Trim = "", ValorDefecto, Valor)
        Catch ex As Exception
            Return ValorDefecto
        End Try
    End Function

    ''' <summary>
    ''' Evalua si un valor no es num�rico y retorna un valor por defecto
    ''' </summary>
    ''' <param name="Valor"></param>
    ''' <param name="ValorDefecto">Valor a retornar en caso de que el valor sea nulo</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SiNoNumerico(ByVal Valor As Object, ByVal ValorDefecto As Object) As Object
        Try
            Return CDbl(Valor)
        Catch ex As Exception
            Return ValorDefecto
        End Try
    End Function

    ''' <summary>
    ''' Retorna un valor Date enviandole una fecha en formato dd/mm/yyyy
    ''' </summary>
    ''' <param name="Fecha">Fecha en formato dd/mm/yyyy</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function OracleDate(ByVal Fecha As String) As Date
        Dim Dia, Mes, Anio As String
        Dia = Fecha.ToString.Substring(0, 2)
        Mes = Fecha.ToString.Substring(3, 2)
        Anio = Fecha.ToString.Substring(6, 4)
        Return New Date(Anio, Mes, Dia)
    End Function

    ''' <summary>
    ''' Convierte una fecha de formato 'mes/dia/a�o' a formato 'dia/mes/a�o' y viceversa
    ''' Nota: La fecha es retornada en una cadena
    ''' </summary>
    ''' <param name="Fecha"></param>
    ''' <param name="FormatoOrigen">0 = 'mes/dia/a�o' y 1 = 'dia/mes/a�o'</param>
    ''' <param name="FormatoDestino">0 = 'mes/dia/a�o' y 1 = 'dia/mes/a�o'</param>
    ''' <param name="IncluirHora">Indica si desea incluir la hora</param>''' 
    ''' <param name="SiErrorRetornar">Si ocurre un error la funci�n retorna este valor y no se retorna la excepci�n con el error</param>
    ''' <param name="MensajeError">Se retorna una excepci�n con este mensaje si ocurre un error y si el par�metro 'SiErrorRetornar' esta vacio</param>
    ''' <param name="RetornarTipoDate">Indica si la fecha a retornar se convertira a tipo de dato 'DATE'</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertirFecha(ByVal Fecha As Object, ByVal FormatoOrigen As FormatoFecha, ByVal FormatoDestino As FormatoFecha, Optional ByVal IncluirHora As Boolean = False, _
                                          Optional ByVal SiErrorRetornar As Object = Nothing, Optional ByVal MensajeError As String = "Error al convertir fecha", Optional ByVal RetornarTipoDate As Boolean = False) As Object
        If Fecha Is Nothing OrElse Fecha Is DBNull.Value OrElse CStr(Fecha) = "" Then Return SiErrorRetornar

        Dim FormatoFecha() As String = {CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, "MM/dd/yyyy", "dd/MM/yyyy"}
        Dim InfoCultura() As String = {CultureInfo.CurrentCulture.Name, "en-US", "es-DO"}
        Dim Formato As String = ""
        Dim Fec As DateTime
        Dim FechaRetorno As String

        Try
            Fec = Date.Parse(Fecha.ToString, New CultureInfo(InfoCultura(FormatoOrigen), True))

            If IncluirHora Then
                If FormatoDestino = OracleAccessLayer.FormatoFecha.Dia_Mes_Ano Then

                    FechaRetorno = Fec.ToString(FormatoFecha(FormatoDestino) & " hh:mm tt").ToLower.Replace("am", "a.m.").Replace("pm", "p.m.")
                Else
                    FechaRetorno = Fec.ToString(FormatoFecha(FormatoDestino) & " hh:mm tt")
                End If
            Else
                FechaRetorno = Fec.ToString(FormatoFecha(FormatoDestino))
            End If

            If RetornarTipoDate Then
                Return CDate(FechaRetorno)
            Else
                Return FechaRetorno
            End If
        Catch ex As Exception
            If SiErrorRetornar IsNot Nothing Then
                Return SiErrorRetornar
            Else
                If Not MensajeError = "" Then
                    Throw New Exception(MensajeError)
                Else
                    Throw New Exception(ex.Message)
                End If
            End If
        End Try
    End Function

    ''' <summary>
    ''' Convierte una fecha de formato 'mes/dia/a�o' a formato 'dia/mes/a�o' y viceversa; y la carga en un Objeto/Variable
    ''' Nota: La fecha es retornada en una cadena
    ''' </summary>
    ''' <param name="objFecha">Objeto/Variable en la cual se cargara la fecha</param>
    ''' <param name="Fecha">Fecha a convertir</param>
    ''' <param name="FormatoOrigen">0 = 'mes/dia/a�o' y 1 = 'dia/mes/a�o'</param>
    ''' <param name="FormatoDestino">0 = 'mes/dia/a�o' y 1 = 'dia/mes/a�o'</param>
    ''' <param name="IncluirHora">Indica si desea incluir la hora</param>''' 
    ''' <param name="SiErrorAsignar">Si ocurre un error a 'objFecha' se le asigna este valor y no se retorna la excepci�n con el error</param>
    ''' <param name="MensajeError">Se retorna una excepci�n con este mensaje si ocurre un error y si el par�metro 'SiErrorAsignar' esta vacio</param>
    ''' <remarks></remarks>
    Public Shared Sub ConvertirFecha(ByRef objFecha As Object, ByVal Fecha As Object, ByVal FormatoOrigen As FormatoFecha, ByVal FormatoDestino As FormatoFecha, Optional ByVal IncluirHora As Boolean = False, _
                                     Optional ByVal SiErrorAsignar As Object = Nothing, Optional ByVal MensajeError As String = "Error al convertir fecha")
        If Fecha Is Nothing OrElse Fecha Is DBNull.Value OrElse CStr(Fecha) = "" Then Exit Sub

        Dim FormatoFecha() As String = {CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, "MM/dd/yyyy", "dd/MM/yyyy"}
        Dim InfoCultura() As String = {CultureInfo.CurrentCulture.Name, "en-US", "es-DO"}
        Dim Formato As String = ""
        Dim Fec As DateTime

        Try
            Fec = Date.Parse(Fecha.ToString, New CultureInfo(InfoCultura(FormatoOrigen), True))

            If IncluirHora Then
                If FormatoDestino = OracleAccessLayer.FormatoFecha.Dia_Mes_Ano Then
                    objFecha = Fec.ToString(FormatoFecha(FormatoDestino) & " hh:mm tt").ToLower.Replace("am", "a.m.").Replace("pm", "p.m.")
                Else
                    objFecha = Fec.ToString(FormatoFecha(FormatoDestino) & " hh:mm tt")
                End If
            Else
                objFecha = Fec.ToString(FormatoFecha(FormatoDestino))
            End If
        Catch ex As Exception
            If SiErrorAsignar IsNot Nothing Then
                objFecha = SiErrorAsignar
            Else
                If Not MensajeError = "" Then
                    Throw New Exception(MensajeError)
                Else
                    Throw New Exception(ex.Message)
                End If
            End If
        End Try
    End Sub

#End Region





End Class


