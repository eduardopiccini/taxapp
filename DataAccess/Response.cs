﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess
{
    public class Response
    {
        public int Code { get; set; }

        public String Message { get; set; }

        public String Details { get; set; }
    }
}
