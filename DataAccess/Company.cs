﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OracleAccessLayer;
using Oracle.DataAccess.Client;
using System.Data;

namespace DataAccess
{
    
    public class Company
    {
        public int Id { get; set; }
        public String Name { get; set; } 
    }

    

    public class CompanyDao
    {
        public class SearchRequest
        {
            public int Id { get; set; }
        }

        public class SearchResponse
        {
            public SearchResponse()
            {
                this.Response = new Response();
                this.Company = new List<Company>();
            }

            public Response Response { get; set; }
            public List<Company> Company { get; set; }
        }

        public SearchResponse Search(SearchRequest request)
        {
            SearchResponse response = new SearchResponse();
            StringBuilder query = new StringBuilder();
            OracleCommand cmd = new OracleCommand();
            DataTable dt = null;

            try
            {
                query.Append(" select company_id, campony_name, range ");
                query.Append(" from tc_vw_company ");

                if (request != null)
                {
                    DataManager.AddBindParamCmd(ref cmd, ref query, "company_id", request.Id, OracleDbType.Int32); 
                }

                dt = DataManager.GetDataTable(ref cmd, query.ToString(), String.Empty, String.Empty);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
			        {
                        Company co = new Company();

                        co.Id = Convert.ToInt32(dt.Rows[i]["company_id"].ToString());
                        co.Name = dt.Rows[i]["company_name"].ToString();

                        response.Company.Add(co);
			        }
                }
            }
            catch (Exception ex)
            {
                
            }

            return response;
        }
    }
}
